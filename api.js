export const DEFAULT_MAP_NOTES_API_URL = 'http://localhost:8008';

const fetchWrapper = async (endpoint, options = {}) => {
  const response = await fetch(endpoint, options);

  const contentType = response.headers.get("Content-Type");

  if (response.ok && contentType && contentType.includes("application/json")) {
    return response.json();
  }

  if ([201, 204].includes(response.status)) {
    return true;
  }

  throw new Error("Fetch request failed");
};

function MapNotesAPI(mapNotesApiUrl) {

  this.getMapNotes = async () => {

  }

  this.getMapNote = async () => {

  }

  this.createMapNote = async () => {

  }

  this.deleteMapNote = async () => {

  }

  this.updateMapNoteFeatures = async () => {

  }

  this.getMapNoteFeatures = async () => {
    
  }

 }

export default MapNotesAPI;
